"use strict";

const CONFIG = require("../config");

const fs = require("fs");
const path = require("path");
const prettysize = require("prettysize");
const B2 = new (require("backblaze-b2"))(CONFIG.backblaze);

const Mail = require("../libs/mail");
const Log = require("../libs/logger").create("backup-rethinkdb.log");
const Profiler = new (require("../libs/profiler"))("Start");
const Database = require("../config/rethinkdb");

Database.dump(CONFIG.rethinkdb.db).then(file_name => {
	Profiler.tick("Database.dump");
	return read_dump(file_name);
}).then(dump => {
	Profiler.tick("read_dump");
	return B2.authorize().then(result => {
		if (result.code) {
			throw result;
		}
		
		Profiler.tick("B2.authorize");
		return B2.getUploadUrl(CONFIG.backblaze.bucket);
	}).then(result => {
		if (result.code) {
			throw result;
		}
		
		Profiler.tick("B2.getUploadUrl");
		return B2.uploadFile({
			"uploadAuthToken": result.authorizationToken,
			"uploadUrl": result.uploadUrl,
			"filename": dump.name,
			"data": dump.data
		});
	}).then(result => {
		if (result.code) {
			throw result;
		}
		
		Profiler.tick("B2.uploadFile", prettysize(result.contentLength));
		return remove_dump(dump.name);
	}).then(() => {
		Log(Profiler.tally("remove_dump"));
	});
}).catch(err => {
	Mail.send_error({
		"subject": "Backup RethinkDB Failed",
		"message": Log(Profiler.tally("Error", err))  
	});
});

function read_dump(file_name) {
	const file_path = path.join(PROJECT_ROOT, file_name);
	
	return new Promise((resolve, reject) => {
		fs.readFile(file_path, (err, data) => {
			return err ? reject(err) : resolve({ "name": file_name, "data": data });
		});
	});
}

function remove_dump(file_name) {
	const file_path = path.join(PROJECT_ROOT, file_name);
	
	return new Promise((resolve, reject) => {
		fs.unlink(file_path, err => err ? reject(err) : resolve());
	});
}
