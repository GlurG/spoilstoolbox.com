"use strict";

const readline = require("readline");

module.exports = {
	"prompt": prompt
};

function prompt(text) {
	const iface = create_interface();
	
	return new Promise(resolve => {
		iface.question(text, answer => {
			iface.close();
			resolve(answer);
		});
	});
}

function create_interface() {
	return readline.createInterface({
		"input": process.stdin,
		"output": process.stdout
	});
}