"use strict";

const fs = require("fs");
const util = require("util");
const path = require("path");
const sprintf = util.format;

const CONFIG = require("../config");
const Mailgun = new (require("mailgun-es6"))(CONFIG.mailgun);

module.exports = {
	"send": send,
	"send_template": send_template,
	"send_error": send_error
};

function send(settings) {
	return Mailgun.sendEmail({
		"from": settings.from, "to": settings.to,
		"subject": settings.subject,
		"html": settings.message,
		"o:testmode": ENVS.dev || ENVS.test
	});
}

function send_template(settings) {
	return new Promise((resolve, reject) => {
		const template_path = path.join(SERVER_ROOT, "emails", `${settings.template}.html`);

		fs.readFile(template_path, "utf-8", (err, content) => {
			if (err) {
				reject(err);
			} else {
				settings.from = settings.from || "Spoils Toolbox <do-not-reply@spoilstoolbox.com>";
				settings.message = sprintf.apply(util, [].concat(content).concat(settings.params));
				send(settings).then(resolve).catch(reject);
			}
		});
	});
}

function send_error(settings) {
	return send({
		"from": "System <system@spoilstoolbox.com>",
		"to": "martin.tberger@gmail.com",
		"subject": settings.subject,
		"message": sprintf("<pre>%s</pre>", JSON.stringify(settings.message, null, "\t"))
	});
}