"use strict";

const fs = require("fs");
const path = require("path");
const moment = require("moment");

module.exports = {
	"create": create
};

function create(file_name) {
	const file_path = path.join(PROJECT_ROOT, "logs", file_name);
	
	return function(entry) {
		const now = moment().format("YYYY-MM-DD HH:mm:ss");
		const entry_string = JSON.stringify(entry);
		const message = `[${now}] ${entry_string}\n`;
		
		fs.appendFileSync(file_path, message);
		
		return entry;
	};
}