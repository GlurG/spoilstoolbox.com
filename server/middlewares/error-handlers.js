/* eslint-disable no-unused-vars */

// Inspired by:
// https://www.joyent.com/developers/node/design/errors
// https://github.com/expressjs/express/blob/master/examples/error-pages/index.js

"use strict";

const error = require("../libs/error");
const Log = require("../libs/logger").create("http-errors.log");

module.exports = function(app) {
	// These should be the last two middlewares in the stack
	app.use(error_handler_404);
	app.use(error_handler);
};

// 404 errors needs to handled separately as they require
// a different function signature
function error_handler_404(req, res, next) {
	return next(error.http.not_found());
}

function error_handler(err, req, res, next) {
	const account = req.session.account;
	
	err.req = {
		"ip-address": req.headers['x-forwarded-for'] || req.connection.remoteAddress,
		"user-id": account ? account.id : undefined,
		"url": req.originalUrl,
		"method": req.method
	};

	Log(err);
	res.status(err.status || 500).end();
}
