"use strict";

const bcrypt = require("bcrypt");
const crypto = require("crypto");
const moment = require("moment");

const TABLE_NAME = "accounts";

// Account recovery can be done once every 24h
const RECOVERY_COOLDOWN = 24 * 60 * 60;

// An account is regarded inactive if it hasn't been activated within 5 days
const INACTIVITY_THRESHOLD = 5 * 24 * 60 * 60;

exports.find_by_email = function(email, callback) {
	r.table(TABLE_NAME)
	.getAll(email, { "index": "email" })
	.run().then(result => callback(null, result[0])).error(callback);
};

exports.find_by_username = function(username, callback) {
	r.table(TABLE_NAME).filter(
		r.row("username").downcase().eq(username.toLowerCase())
	).run().then(result => callback(null, result[0])).error(callback);
};

exports.create = function(email, username, password, callback) {
	bcrypt.hash(password, 10, (err, hash) => {
		if (err) {
			return callback(err);
		}

		const instance = {
			"email": email,
			"username": username,
			"password": hash,
			"created_at": moment().unix(),
			"activated": ENVS.dev,
			"activation_token": crypto.randomBytes(32).toString("hex")
		};

		r.table(TABLE_NAME).insert(instance).run().then(result => {
			callback(result.first_error, instance.activation_token);
		}).error(callback);
	});
};

exports.activate = function(token, callback) {
	r.table(TABLE_NAME)
	.getAll(token, { "index": "activation_token" })
	.update({
		"activated": true,
		"activation_token": r.literal(),
		"activated_at": moment().unix()
	}).run().then(result => {
		callback(result.first_error, result.replaced === 1);
	}).error(callback);
};

exports.authenticate = function(username, password, callback) {
	r.table(TABLE_NAME)
	.getAll(username, { "index": "username" })
	.run().then(result => {
		const account = result[0];

		if (!account) {
			return callback(null, "ACCOUNT.INVALID_CREDENTIALS");
		} else if (!account.activated) {
			return callback(null, "ACCOUNT.NOT_ACTIVATED");
		}

		bcrypt.compare(password, account.password, (err, match) => {
			callback(err, match ? null : "ACCOUNT.INVALID_CREDENTIALS", account);
		});
	}).error(callback);
};

exports.set_recovery_token = function(account_id, callback) {
	crypto.randomBytes(32, (err, buf) => {
		if (err) {
			return callback(err);
		}

		const recovery_token = buf.toString("hex");

		r.table(TABLE_NAME).get(account_id).update(account => {
			return r.branch(
				r.now().toEpochTime().ge(account("recovery_at").default(0)),
				account.merge({
					"recovery_at": r.now().add(RECOVERY_COOLDOWN).toEpochTime(),
					"recovery_token": recovery_token
				}),
				account
			);
		}, { "returnChanges": true }).run().then(result => {
			if (result.replaced === 1) {
				callback(result.first_error, null, result.changes[0].new_val);
			} else if (result.unchanged === 1) {
				callback(result.first_error, "ACCOUNT.RECOVERY_COOLDOWN");
			} else {
				// Not sure if we can end up here.
				// Return an error so it gets logged.
				callback(new Error("Interesting Error"));
			}
		}).error(callback);
	});
};

exports.set_password = function(token, password, callback) {
	bcrypt.hash(password, 10, (err, hash) => {
		if (err) {
			return callback(err);
		}

		r.table(TABLE_NAME)
		.getAll(token, { "index": "recovery_token" })
		.update({
			"recovery_token": r.literal(),
			"recovery_at": r.literal(),
			"password": hash
		}).run().then(result => {
			callback(
				result.first_error,
				result.replaced === 0 ? "ACCOUNT.INVALID_RECOVERY_TOKEN" : null
			);
		}).error(callback);
	});
};

exports.change_password = function(account_id, current_password, new_password, callback) {
	r.table(TABLE_NAME).get(account_id).run().then(account => {
		bcrypt.compare(current_password, account.password, (err, match) => {
			if (err) {
				return callback(err);
			} else if (!match) {
				return callback(null, "ACCOUNT.CURRENT_PASSWORD_MISMATCH");
			}

			bcrypt.hash(new_password, 10, (err, hash) => {
				r.table(TABLE_NAME).get(account_id).update({
					"password": hash
				}).run().then(result => {
					callback(result.first_error);
				}).error(callback);
			});
		});
	}).error(callback);
};

exports.purge_inactive = function(callback) {
	r.table(TABLE_NAME).filter(
		r.row("activated").eq(false).and(
			r.row("created_at").add(INACTIVITY_THRESHOLD).lt(r.now().toEpochTime())
		)
	).delete().run().then(result => {
		callback(result.first_error, result);
	}).error(callback);
};
