"use strict";

const moment = require("moment");

const TABLE_NAME = "decks";

exports.find_by_query = function({ username, trades, cards }, callback) {
	// We might need to optimize this query with one or more compound indexes:
	// https://www.rethinkdb.com/docs/secondary-indexes/javascript/
	
	let r_query =
		r.table(TABLE_NAME)
		.getAll(true, { "index": "published" });
	
	if (username) {
		r_query = r_query.filter(
			r.row("username").downcase().eq(username.toLowerCase())
		);
	}
	
	if (trades && trades.length > 0) {
		r_query = r_query.filter(
			r.row("threshold").match(trades.join("|"))
		);
	}
	
	if (cards && cards.length > 0) {
		r_query = r_query.filter(
			r.row("cards").contains(card_id => r.expr(cards).contains(card_id))
		);
	}
	
	r_query
	.orderBy(r.desc("published_at"))
	.pluck("id", "name", "username", "threshold", "published_at")
	.run().then(decks => callback(null, decks)).error(callback);
};

exports.find_by_recency = function(start, end, callback) {
	r.table(TABLE_NAME)
	.getAll(true, { "index": "published" })
	.orderBy(r.desc("published_at"))
	.slice(start, end)
	.pluck("id", "name", "username", "threshold", "published_at")
	.run().then(decks => callback(null, decks)).error(callback);
};

exports.find_by_username = function(username, start, end, callback) {
	r.table(TABLE_NAME)
	.getAll(username, { "index": "username" })
	.orderBy(r.desc("published_at"))
	.slice(start, end)
	.pluck("id", "name", "published", "threshold", "created_at", "updated_at")
	.run().then(decks => callback(null, decks)).error(callback);
};

exports.find_by_id = function(id, callback) {
	r.table(TABLE_NAME).get(id)
	.pluck("id", "name", "username", "cards").default({})
	.run().then(deck => callback(null, deck)).error(callback);
};

exports.create = function(deck, callback) {
	deck.created_at = moment().unix();

	r.table(TABLE_NAME).insert(deck).run().then(result => {
		callback(result.first_error);
	}).error(callback);
};

exports.update = function(deck, callback) {
	deck.updated_at = moment().unix();

	r.table(TABLE_NAME)
	.getAll([deck.id, deck.username], { "index": "id_username" })
	.update(deck).run().then(result => {
		const replaced = result.replaced === 1;
		const unchanged = result.unchanged === 1;
		callback(result.first_error, replaced || unchanged);
	}).error(callback);
};

exports.publish = function(id, username, callback) {
	r.table(TABLE_NAME)
	.getAll([id, username], { "index": "id_username" })
	.update({
		"published": true,
		"published_at": r.row("published_at").default(moment().unix())
	}).run().then(result => {
		callback(result.first_error, result.replaced === 1);
	}).error(callback);
};

exports.unpublish = function(id, username, callback) {
	r.table(TABLE_NAME)
	.getAll([id, username], { "index": "id_username" })
	.update({ "published": false }).run().then(result => {
		callback(result.first_error, result.replaced === 1);
	}).error(callback);
};

exports.trash = function(id, username, callback) {
	r.table(TABLE_NAME)
	.getAll([id, username], { "index": "id_username" })
	.delete().run().then(result => {
		callback(result.first_error, result.deleted === 1);
	});
};
