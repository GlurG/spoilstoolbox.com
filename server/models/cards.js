"use strict";
var util = require('util');
const sprintf = require("util").format;
const tools = require("../libs/tools");

exports.find_by_query = function(query, callback) {
	const elastic_query = parse_query(query);

	if (elastic_query instanceof Error) {
		return callback(elastic_query);
	}

	submit_query(elastic_query, query.options, callback);
};

function parse_query(query) {
	try {
		const im_query = { "sets": [] };
		let query_string = parse_filters(im_query, query.filters);
		query_string = sprintf("%s %s", query_string, query.query_string || "");
		const tokens = tokenize(query_string);
		parse_tokens(im_query, tokens);

		return make_elastic_query(im_query, query.options, query.params);
	} catch (err) {
		return err;
	}
}

const UUID_REGEXP = /^[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/i;

// Splits the filters into two parts:
// 1) String values that can be simply appended to the query string
// 2) Set indices, that need to be treated differently
function parse_filters(im_query, filters) {
	let query_string = "";
	
	filters.forEach(function(filter) {
		if (UUID_REGEXP.test(filter)) {
			im_query.sets.push(filter);
		} else {
			query_string = sprintf("%s %s", query_string, filter);
		}
	});

	return query_string;
}

function tokenize(str) {
	str = str.toLowerCase().trim();

	const OPERATORS = { ">": 1, ">=": 1, "=": 1, "<=": 1, "<": 1 };

	function is_operator(c) {
		return c == "<" || c == "=" || c == ">";
	}

	function is_whitespace(c) {
		return c == " ";
	}

	const tokens = [];

	for (let i = 0; i < str.length;) {
		let c = str[i];

		if (c == '"') {
			const value_start = i;

			do {
				c = str[++i];

				if (c === undefined) {
					const message = sprintf("Missing closing %s in search query", '"');
					throw new Error(message);
				}
			} while (c != '"');

			const value = str.slice(value_start + 1, i++).trim();
			tokens.push({ "type": "string", "value": value });
		} else if (is_operator(c)) {
			let value = "";

			while (OPERATORS[value.concat(c)]) {
				value = value.concat(c);
				c = str[++i];
			}

			if (OPERATORS[value] === undefined) {
				const message = sprintf("Invalid operator '%s'", value);
				throw new Error(message);
			}

			tokens.push({ "type": "operator", "value": value });
		} else if (!is_whitespace(c)) {
			const value_start = i;

			while (!is_whitespace(c) && !is_operator(c) && c !== undefined) {
				c = str[++i];
			}

			const value = str.slice(value_start, i);

			if (/^-?\d+$/.test(value)) {
				tokens.push({ "type": "number", "value": parseInt(value, 10) });
			} else {
				tokens.push({ "type": "id", "value": value });
			}
		} else if (is_whitespace(c)) {
			i++;
		} else {
			const message = sprintf("Unrecognized token '%s'", c);
			throw new Error(message);
		}
	}

	return tokens;
}

const KEYWORDS = [
	{ type: "trade",		value: "arcanist" },
	{ type: "trade",		value: "banker" },
	{ type: "trade",		value: "gearsmith" },
	{ type: "trade",		value: "rogue" },
	{ type: "trade",		value: "universal" },
	{ type: "trade",		value: "warlord" },

	{ type: "type",			value: "character" },
	{ type: "type",			value: "faction" },
	{ type: "type",			value: "item" },
	{ type: "type",			value: "location" },
	{ type: "type",			value: "resource" },
	{ type: "type",			value: "tactic" },

	{ type: "rarity",		value: "common" },
	{ type: "rarity",		value: "uncommon" },
	{ type: "rarity",		value: "rare" },
	{ type: "rarity",		value: "uber" },
	{ type: "rarity",		value: "fixed" },
	{ type: "rarity",		value: "super-rare" },
	{ type: "rarity",		value: "ultra-rare" },

	{ type: "attribute",	value: "cost" },
	{ type: "attribute",	value: "threshold" },
	{ type: "attribute",	value: "strength" },
	{ type: "attribute",	value: "life" },
	{ type: "attribute",	value: "speed" },
	{ type: "attribute",	value: "structure" },

	{ type: "format",		value: "block" },
	{ type: "format",		value: "current" },
	{ type: "format",		value: "chronicles" }
];

function parse_tokens(im_query, tokens) {
	for (let i = 0; i < tokens.length;) {
		const token = tokens[i];

		switch (token.type) {
			case "id": {
				i += parse_id(im_query, tokens, i);
				break;
			}

			case "number": {
				// Treat stray numbers as IDs
				// This is unlikely to happen as most people would type in the alphabetic form (1337! -> leet!)
				tokens[i] = { "type": "id", "value": token.value.toString() };
				i += parse_id(im_query, tokens, i);
				break;
			}

			case "string": {
				i += parse_text(im_query, token);
				break;
			}

			default: {
				const message = sprintf("Malformed search query near '%s'", token.value);
				throw new Error(message);
			}
		}
	}
}

function parse_id(im_query, tokens, i) {
	function token_to_keyword(value) {
		for (let index in KEYWORDS) {
			const keyword = KEYWORDS[index];

			if (value == keyword.value) {
				return keyword;
			}
		}
	}

	const token = tokens[i];
	const keyword = token_to_keyword(token.value);

	if (keyword) {
		return parse_keyword(im_query, tokens, i, keyword);
	} else {
		return parse_text(im_query, token);
	}
}

function parse_text(im_query, token) {
	function escape(str) {
		// https://www.elastic.co/guide/en/elasticsearch/reference/current/query-dsl-query-string-query.html
		// Everything but " and - is escaped
		const RESERVED = /[\+\=\&\|\>\!\(\)\{\}\[\]\^\~\*\?\:\\\/]/g;
		return str.replace(RESERVED, "\\$&");
	}

	function to_leet_speak(str) {
		const LOOKUP = { "o": "0", "i": "1", "l": "1", "e": "3", "a": "4", "s": "5", "b": "6", "t": "7", "g": "9" };

		return str.replace(/[oileasbtg]/g, function(c) {
			return LOOKUP[c];
		});
	}

	// Removes characters which might screw up the query_string search
	const text = escape(token.value);
	const value = token.type == "id" ? text + "*" : '"' + text + '"';

	im_query.text = im_query.text || [];
	im_query.text.push(`(${value} OR ${to_leet_speak(value)})`);

	return 1;
}

function parse_keyword(im_query, tokens, i, keyword) {
	switch (keyword.type) {
		case "trade":
		case "type":
		case "rarity":
		case "format":
		case "staple": {
			im_query[keyword.type] = im_query[keyword.type] || [];
			im_query[keyword.type].push(keyword.value);

			return 1;
		}

		case "attribute": {
			const attribute = keyword.value;
			const operator = tokens[i + 1];
			const number = tokens[i + 2];

			if (operator === undefined) {
				// Treat stray keywords as free text
				return parse_text(im_query, keyword);
			} else if (operator.type != "operator") {
				const message = sprintf("Operator expected after '%s'", attribute);
				throw new Error(message);
			}

			if (number === undefined || number.type != "number") {
				const message = sprintf("Number expected after '%s %s'", attribute, operator.value);
				throw new Error(message);
			}

			im_query[attribute] = im_query[attribute] || [];
			im_query[attribute].push({ operator: operator.value, operand: number.value });

			return 3;
		}
	}
}

const QUERY_TEMPLATE = {
	"index": "spoils",
	"type": "card",
	"search_type": "dfs_query_then_fetch",
	"body": {
		"size": 10000,
		"query": {
			"filtered": {
				"filter": {
					"bool": {
						"must": [],
						"must_not": []
					}
				}
			}
		},
		"sort": [
			"_score",
			{ "name.raw": "asc" },
			{ "set.order": "desc" }
		]
	}
};

function make_elastic_query(im_query, options, params) {
	const query = tools.clone(QUERY_TEMPLATE);

	set_projection(query, params.projection);
	set_sort_order(query, options);

	if (!options.include_alt_art) {
		query.body.query.filtered.filter.bool.must_not.push({
			"exists": { "field": "alt-art" }
		});
	}

	for (let keyword in im_query) {
		const values = im_query[keyword];

		switch (keyword) {
			case "trade":
			case "type":
			case "rarity": {
				const query_part = { "terms": {} };
				query_part.terms[keyword] = values;
				query.body.query.filtered.filter.bool.must.push(query_part);
				break;
			}

			case "format": {
				let legality = "Yes";

				if (options.include_bans) {
					legality += " Banned";
				}

				if (options.include_specials) {
					legality += " Special";
				}

				query.body.query.filtered.filter.bool.must.push({
					"nested": {
						"path": "format",
						"query": {
							"bool": {
								"must": [
									{ "match": { "format.name": values[0] }},
									{ "match": { "format.legal": { "query": legality, "operator": "OR" }}}
								]
							}
						}
					}
				});

				break;
			}

			case "sets": {
				if (values.length > 0) {
					query.body.query.filtered.filter.bool.must.push({
						"terms": { "set.id": values }
					});
				}

				break;
			}

			case "threshold": {
				let regexp;
				const interval = parse_interval(values);

				switch (interval.type) {
					case "equal":		regexp = sprintf("{%d}", interval.equal); break;
					case "range":		regexp = sprintf("{%d,%d}", interval.lower_bound, interval.upper_bound); break;
					case "lower_bound":	regexp = sprintf("{%d,}", interval.lower_bound); break;
					case "upper_bound": regexp = sprintf("{0,%d}", interval.upper_bound); break;
				}

				query.body.query.filtered.filter.bool.must.push({
					"regexp": { "threshold": sprintf(".%s", regexp) }
				});

				break;
			}

			case "cost":
			case "strength":
			case "life":
			case "speed":
			case "structure": {
				const query_expr = {};
				const interval = parse_interval(values);

				switch (interval.type) {
					case "equal":
						query_expr.term = {};
						query_expr.term[keyword] = interval.equal;
						break;

					case "range":
						query_expr.range = {};
						query_expr.range[keyword] = { "lte": interval.upper_bound, "gte": interval.lower_bound };
						break;

					case "lower_bound":
						query_expr.range = {};
						query_expr.range[keyword] = { "gte": interval.lower_bound };
						break;

					case "upper_bound":
						query_expr.range = {};
						query_expr.range[keyword] = { "lte": interval.upper_bound };
						break;
				}

				query.body.query.filtered.filter.bool.must.push(query_expr);
				break;
			}

			case "text": {
				query.body.query.filtered.query = query.body.query.filtered.query || [];
				query.body.query.filtered.query.push({
					"query_string": {
						"analyzer": "whitespace",
						"use_dis_max": false,
						"query": values.join(" AND "),
						"fields": [
							"name^5",
							"supertype^4",
							"subtype^3",
							"meta.keywords^2",
							"meta.abilities.name^2",
							"meta.abilities.effect^2",
							"meta.effects^2",
							"meta.triggers^2",
							"meta.conditionals^2",
							"meta.abilities.cost",
							"meta.extra_costs"
						],
						"analyze_wildcard": true
					}
				});

				break;
			}
		}
	}

	return query;
}

function parse_interval(values) {
	const interval = {};

	for (let i in values) {
		const value = values[i];

		switch (value.operator) {
			case "<":	value.operand = Math.max(value.operand - 1, 0);
			/* falls through */
			case "<=":	interval.upper_bound = interval.upper_bound ? Math.min(interval.upper_bound, value.operand) : value.operand;
				break;
			case "=":	interval.equal = value.operand;
				break;
			case ">":	value.operand++;
			/* falls through */
			case ">=":	interval.lower_bound = interval.lower_bound ? Math.max(interval.lower_bound, value.operand) : value.operand;
				break;
		}
	}

	if (interval.equal !== undefined) {
		interval.type = "equal";
	} else if (interval.lower_bound !== undefined && interval.upper_bound !== undefined) {
		interval.type = "range";
	} else if (interval.lower_bound !== undefined) {
		interval.type = "lower_bound";
	} else {
		interval.type = "upper_bound";
	}

	return interval;
}

function set_projection(query, projection) {
	switch (projection) {
		case "simple":
			query.body._source = ["id", "name"];
			break;

		case "extended":
			query.body._source = ["id", "name", "trade", "supertype", "type", "cost", "threshold", "format", "set.name"];
			break;

		case "full":
			// No projection = every field gets included
			delete query.body._source;
			break;
		
		default:
			// Custom projection
			query.body._source = projection;
			break;
	}
}

function set_sort_order(query, options) {
	const sort_order = [];

	for (let i in options) {
		switch (options[i]) {
			case "sort_by_cost":
				sort_order.push({ "cost": "asc" });
				break;

			case "sort_by_threshold":
				sort_order.push({ "_script": { "script": { "file": "sort_by_threshold" }, "type": "number", "order": "asc" } });
				break;

			case "sort_by_strength":
				sort_order.push({ "strength": "asc" });
				break;

			case "sort_by_life":
				sort_order.push({ "life": "asc" });
				break;

			case "sort_by_speed":
				sort_order.push({ "speed": "asc" });
				break;

			case "sort_by_structure":
				sort_order.push({ "structure": "asc" });
				break;
		}
	}

	// If we have a custom sort order...
	if (sort_order.length > 0) {
		sort_order.push({ "name.raw": "asc" });
		sort_order.push({ "set.order": "desc" });
		query.body.sort = sort_order;
	}
}

function submit_query(elastic_query, options, callback) {
	Elasticsearch.search(elastic_query, (err, response) => {
		if (err) {
			callback(err);
		} else if (response.hits.total === 0) {
			callback();
		} else {
			if (!options.include_reprints) {
				response.hits.hits = group_by_name(response.hits.hits);
			}

			callback(null, filter_source(response.hits.hits));
		}
	});
}

// This can be done in ES natively, but the query is quite convoluted
// and is about 2-2.5x slower.
function group_by_name(hits) {
	const seen = {};

	return hits.reduce((result, card) => {
		const card_name = card._source.name;

		if (!seen[card_name]) {
			seen[card_name] = true;
			result.push(card);
		}

		return result;
	}, []);
}

// This should be possible to filter out in ES directly, alas it is not.
// http://stackoverflow.com/questions/28303134/return-only-source-from-a-search
function filter_source(hits) {
	return hits.map(c => {
		c = c._source;
		return c;
	});
}

exports.find_by_name = function(name, params, callback) {
	find_by_names([name], params, callback);
};

exports.find_by_names = function(names, params, callback) {
	find_by_names(names, params, callback);
};

function find_by_names(names, params, callback) {
	const elastic_query = make_elastic_query({}, {}, params);
	elastic_query.body.query.filtered.filter.bool.must.push({
		"terms": {
			"name.raw": names
		}
	});
	
	submit_query(elastic_query, {}, callback);
}

exports.find_by_ids = function(ids, callback) {
	const PARAMS = { "projection": "extended" };
	const elastic_query = make_elastic_query({}, {}, PARAMS);
	elastic_query.body.query.ids = {
		"type": "card",
		"values": ids
	};

	// "ids"-query doesn't like having anything else beside it...
	delete elastic_query.body.sort;
	delete elastic_query.body.query.filtered;
	submit_query(elastic_query, {}, callback);
};
