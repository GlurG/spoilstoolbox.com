"use strict";

const tools = require("../libs/tools");

function init(options) {
	// https://github.com/elastic/elasticsearch-js/issues/33
	global.Elasticsearch = new require("elasticsearch").Client(tools.clone(options));

	return new Promise((resolve, reject) => {
		if (ENVS.prod) {
			Elasticsearch.ping(null, err => err ? reject(err) : resolve());
		} else {
			resolve();
		}
	});
}

function shutdown() {
	return new Promise(resolve => resolve());
}

module.exports = {
	"init": init,
	"shutdown": shutdown
};
