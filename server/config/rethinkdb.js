"use strict";

const _ = require("lodash");
const exec = require('child_process').exec;

function init(options) {
	global.r = require("rethinkdbdash")(options);

	return new Promise((resolve, reject) => {
		ensure_database(options.db).then(
			() => ensure_tables(options.db, options.schema.tables)
		).then(
			() => ensure_indexes(options.db, options.schema.indexes)
		).then(
			() => wait(options.db)
		).then(() => {
			if (ENVS.test) {
				return empty(options.db);
			} else {
				resolve();
			}
		}).then(resolve).catch(reject);
	});
}

function ensure_database(name) {
	const db_exists = r.dbList().contains(name);
	const db_create = r.dbCreate(name);

	return new Promise((resolve, reject) => {
		r.branch(db_exists, null, db_create)
		.run().then(resolve).error(reject);
	});
}

function ensure_tables(db_name, tables) {
	const promises = [];

	_.forEach(tables, function(table_name) {
		const promise = ensure_table(db_name, table_name);
		promises.push(promise);
	});

	return Promise.all(promises);
}

function ensure_table(db_name, table_name) {
	const table_exists = r.db(db_name).tableList().contains(table_name);
	const table_create = r.db(db_name).tableCreate(table_name);

	return new Promise((resolve, reject) => {
		r.branch(table_exists, [], table_create)
		.run().then(resolve).error(reject);
	});
}

function ensure_indexes(db_name, indexes) {
	const promises = [];

	_.forEach(indexes, function(table_indexes, table_name) {
		_.forEach(table_indexes, function(index) {
			const promise = ensure_index(db_name, table_name, index);
			promises.push(promise);
		});
	});

	return Promise.all(promises);
}

function ensure_index(db_name, table_name, index) {
	const index_exists = r.db(db_name).table(table_name).indexList().contains(index.name);
	let index_create;

	if (index.props instanceof Array) {
		// Compound
		index_create = r.db(db_name).table(table_name).indexCreate(
			index.name, index.props.map(field => r.row(field))
		);
	} else if (index.props instanceof Object) {
		// multi, geo
		index_create = r.db(db_name).table(table_name).indexCreate(
			index.name, index.props
		);
	} else {
		// simple
		index_create = r.db(db_name).table(table_name).indexCreate(index.name);
	}

	return new Promise((resolve, reject) => {
		r.branch(index_exists, [], index_create)
		.run().then(resolve).error(reject);
	});
}

function wait(db_name) {
	return new Promise((resolve, reject) => {
		r.db(db_name).wait().run().then(resolve).error(reject);
	});
}

function empty(db_name) {
	return new Promise((resolve, reject) => {
		r.db(db_name).tableList().forEach(table => {
			return r.db(db_name).table(table).delete();
		}).run().then(resolve).error(reject);
	});
}

function shutdown() {
	return new Promise((resolve, reject) => {
		r.getPoolMaster().drain().then(() => resolve()).error(reject);
	});
}

function dump(db_name) {
	const moment = require("moment");
	const timestamp = moment().format("YYYY-MM-DD_HH-mm-ss");
	const file_name = `${timestamp}.tar.gz`;
	const command = `rethinkdb dump -e ${db_name} -f ${file_name}`;
	
	return new Promise((resolve, reject) => {
		exec(command, (err, stdout, stderr) => {
			if (err || stderr) {
				reject({ "err": err, "stderr": stderr });
			} else {
				resolve(file_name);
			}
		});
	}); 
}

function restore(file_name) {
	const command = `rethinkdb restore ${file_name} --force`;
	
	return new Promise((resolve, reject) => {
		exec(command, (err, stdout, stderr) => {
			if (err || stderr) {
				reject({ "err": err, "stderr": stderr });
			} else {
				resolve(file_name);
			}
		});
	});
}

module.exports = {
	"init": init,
	"shutdown": shutdown,
	"dump": dump,
	"restore": restore
};
