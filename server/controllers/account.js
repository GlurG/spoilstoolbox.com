"use strict";

const crypto = require("crypto");

const error = require("../libs/error");
const Mail = require("../libs/mail");
const Accounts = require("../models/accounts");

function validate_create(req, res, next) {
	req.sanitizeBody("email").normalizeEmail();
	req.checkBody("email", "ACCOUNT.EMAIL_INVALID").isEmail();
	req.checkBody("email", "ACCOUNT.EMAIL_DISPOSABLE").isNotDisposableEmail();
	req.checkBody("email", "ACCOUNT.EMAIL_REGISTERED").isAsyncPredicateFalse(Accounts.find_by_email);
	req.checkBody("username", "ACCOUNT.USERNAME_INVALID_LENGTH").len(3, 12);
	req.checkBody("username", "ACCOUNT.USERNAME_ALNUM_ONLY").isAlphanumeric();
	req.checkBody("username", "ACCOUNT.USERNAME_REGISTERED").isAsyncPredicateFalse(Accounts.find_by_username);
	req.checkBody("password", "ACCOUNT.PASSWORD_TOO_SHORT").len(8);

	req.asyncValidationErrors().then(() => next()).catch(errors => {
		res.status(400).json(error.form(errors));
	});
}

function create(req, res, next) {
	const email = req.body.email;
	const username = req.body.username;
	const password = req.body.password;
	Accounts.create(email, username, password, (err, activation_token) => {
		if (err) {
			return next(error.http.internal(err));
		} else if (ENVS.dev || ENVS.test) {
			return res.status(201).end();
		}
		
		const url = `${req.protocol}://www.spoilstoolbox.com/#!/account/activate/${activation_token}`;
		Mail.send_template({
			"template": "welcome",
			"params": [username, url],
			"to": email,
			"subject": "Welcome to Spoils Toolbox!"
		}).then(() => {
			res.status(201).end();
		}).catch(err => next(error.http.internal(err)));
	});
}

exports.create = [validate_create, create];

exports.activate = function(req, res, next) {
	const token = req.body.token;
	Accounts.activate(token, (err, activated) => {
		if (err) {
			return next(error.http.internal(err));
		} else if (!activated) {
			res.status(400).end();
		} else {
			res.status(200).end();
		}
	});
};

function validate_authenticate(req, res, next) {
	req.checkBody("username", "ACCOUNT.USERNAME_INVALID_LENGTH").len(3, 12);
	req.checkBody("username", "ACCOUNT.USERNAME_ALNUM_ONLY").isAlphanumeric();
	req.checkBody("password", "ACCOUNT.PASSWORD_TOO_SHORT").len(8);

	req.asyncValidationErrors().then(() => next()).catch(errors => {
		res.status(400).json(error.form(errors));
	});
}

function authenticate(req, res, next) {
	const username = req.body.username;
	const password = req.body.password;
	Accounts.authenticate(username, password, (err, error_code, account) => {
		if (err) {
			return next(error.http.internal(err));
		} else if (error_code) {
			return res.status(400).json(error_code);
		}
		
		req.session.authed = true;
		req.session.account = account;
		req.session.csrf_token = crypto.randomBytes(16).toString("hex"); 

		res.status(200).json({
			"username": account.username,
			"csrf_token": req.session.csrf_token
		});
	});
}

exports.authenticate = [validate_authenticate, authenticate];

function validate_recover(req, res, next) {
	const value = req.body.email_or_username;
	req.sanitizeBody("email_or_username").normalizeEmail();
	const sanitized_value = req.body.email_or_username;

	if (sanitized_value) {
		// We have a valid email
		Accounts.find_by_email(sanitized_value, (err, account) => {
			if (err) {
				next(error.http.internal(err));
			} else if (!account) {
				res.status(400).json(error.form_field("email_or_username", "ACCOUNT.BY_EMAIL_NOT_FOUND"));
			} else {
				req.account_id = account.id;
				next();
			}
		});
	} else {
		// Assume it is a username
		Accounts.find_by_username(value, (err, account) => {
			if (err) {
				next(error.http.internal(err));
			} else if (!account) {
				res.status(400).json(error.form_field("email_or_username", "ACCOUNT.BY_USERNAME_NOT_FOUND"));
			} else {
				req.account_id = account.id;
				next();
			}
		});
	}
}

function recover(req, res, next) {
	Accounts.set_recovery_token(req.account_id, (err, error_code, account) => {
		if (err) {
			return next(error.http.internal(err));
		} else if (error_code) {
			return res.status(400).json(error_code);
		} else if (ENVS.dev || ENVS.test) {
			return res.status(200).end();
		}
		
		const url = `${req.protocol}://www.spoilstoolbox.com/#!/account/set-password/${account.recovery_token}`;
		Mail.send_template({
			"template": "recover",
			"params": [account.username, url],
			"to": account.email,
			"subject": "Spoils Toolbox Account Recovery"
		}).then(() => {
			res.status(200).end();
		}).catch(err => next(error.http.internal(err)));
	});
}

exports.recover = [validate_recover, recover];

function validate_set_password(req, res, next) {
	req.checkBody("password", "ACCOUNT.PASSWORD_TOO_SHORT").len(8);

	req.asyncValidationErrors().then(() => next()).catch(errors => {
		res.status(400).json(error.form(errors));
	});
}

function set_password(req, res, next) {
	const token = req.body.token;
	const password = req.body.password;
	Accounts.set_password(token, password, (err, error_code) => {
		if (err) {
			next(error.http.internal(err));
		} else if (error_code) {
			res.status(400).json(error_code);
		} else {
			res.status(200).end();
		}
	});
}

exports.set_password = [validate_set_password, set_password];

function validate_change_password(req, res, next) {
	req.checkBody("current_password", "ACCOUNT.PASSWORD_TOO_SHORT").len(8);
	req.checkBody("new_password", "ACCOUNT.PASSWORD_TOO_SHORT").len(8);

	req.asyncValidationErrors().then(() => next()).catch(errors => {
		res.status(400).json(error.form(errors));
	});
}

function change_password(req, res, next) {
	const account_id = req.session.account.id;
	const current_password = req.body.current_password;
	const new_password = req.body.new_password;
	Accounts.change_password(account_id, current_password, new_password, (err, error_code) => {
		if (err) {
			next(error.http.internal(err));
		} else if (error_code) {
			res.status(400).json(error_code);
		} else {
			res.status(200).end();
		}
	});
}

exports.change_password = [validate_change_password, change_password];

exports.settings = function(req, res) {
	const account = req.session.account;
	
	res.status(200).json({
		"email": account.email,
		"username":	account.username
	});
};
