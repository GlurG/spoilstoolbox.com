"use strict";

const xml2js = require("xml2js");
const error = require("../libs/error");
const Cards = require("../models/cards");

exports.proxies = function(req, res) {
	res.render("deck-builder/proxies");
};

exports.parse_from_file = function(req, res, next) {
	const callback = function(err, card_names) {
		if (err) {
			return next(error.http.internal(err));
		}
		
		const OPTIONS = { "projection": "extended" };
		Cards.find_by_names(card_names, OPTIONS, (err, cards) => {
			if (err) {
				next(error.http.internal(err));
			} else {
				const mapped = card_names.map(card_name => {
					return cards.find(card => card.name === card_name);
				});
				
				res.status(200).json(mapped);
			}
		});
	};

	switch (req.body.extension) {
		case "txt":
			parse_from_txt(req.body.content, callback);
			break;

		case "o8d":
			parse_from_o8d(req.body.content, callback);
			break;

		default:
			res.status(400).json("DECK.INVALID_FILE_FORMAT");
			break;
	}
};

function parse_from_txt(content, callback) {
	function try_parse_faction(line) {
		const faction = /Faction: (.*)/.exec(line);

		if (faction) {
			const faction_name = faction[1];
			return faction_name === "None" ? null : faction_name;
		}
	}

	function try_parse_stack(line) {
		const stack = /(\d+)(\s+)(.*)/.exec(line);

		if (stack) {
			const name = stack[3].trim();
			const copies = parseInt(stack[1], 10);

			return Array(copies).fill(name);
		} else {
			return [];
		}
	}

	let cards = [];

	content.split("\n").forEach(line => {
		if (line.length <= 1) {
			return;
		}

		const faction = try_parse_faction(line);

		if (faction) {
			return cards.push(faction);
		}

		cards = cards.concat(try_parse_stack(line));
	});

	callback(null, cards);
}

function parse_from_o8d(content, callback) {
	xml2js.parseString(content, (err, result) => {
		if (err) {
			callback(err);
		} else {
			let cards = [];
			
			result.deck.section[0].card.forEach(card => {
				const name = card._;
				const copies = parseInt(card.$.qty, 10);
				cards = cards.concat(Array(copies).fill(name));
			});

			result.deck.section[1].card.forEach(card => {
				const name = card._;
				const copies = parseInt(card.$.qty, 10);
				cards = cards.concat(Array(copies).fill(name));
			});

			callback(null, cards);
		}
	});
}
