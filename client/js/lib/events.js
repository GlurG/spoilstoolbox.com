import Vue from "vue";

// Global event bus, as per recommendation:
// https://github.com/vuejs/vue/wiki/2.0-features

class Events {
	constructor() {
		this.vue = new Vue();
	}
	
	on(event_name, callback) {
		this.vue.$on(event_name, callback);
	}
	
	emit(event_name, data) {
		this.vue.$emit(event_name, data);
	}
}

export default new Events;
