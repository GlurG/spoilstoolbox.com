import PNotify from "pnotify";

PNotify.prototype.options.styling = "bootstrap3";

export { display, clear };

function display(message) {
	const options = {
		"text":		message.text,
		"type":		message.type,
		"icon":		false,
		"hide":		default_hide(message.hide),
		"delay":	default_delay(message.type),
		"buttons": {
			"sticker":	false,
			"closer":	true
		}
	};
	
	if (message.title) {
		options.title = message.title;
		options.icon = default_icon(message.type);
	}
	
	new PNotify(options);
}

function default_hide(value) {
	return value !== undefined ? value : true;
}

function default_delay(value) {
	switch (value) {
		case "success":	return 3 * 1000;
		case "warning": return 5 * 1000;
		case "error":	return 10 * 1000;
	}
}

function default_icon(value) {
	switch (value) {
		case "success":	return "fa fa-check-circle-o fa-lg";
		case "warning":	return "fa fa-exclamation-triangle fa-lg";
		case "error":	return "fa fa-times-circle-o fa-lg";
	}
}

function clear() {
	PNotify.removeAll();
}
