import $ from "jquery";

import Modal from "./modal.js";
import Template from "./card-search-help.html";

export default {
	"template": Template,
	"mixins": [Modal],
	"methods": {
		"submit": submit
	}
};

function submit(e) {
	this.$emit("example-clicked", $(e.target).text());
	this.close();
}
