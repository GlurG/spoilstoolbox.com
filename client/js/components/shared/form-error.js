export default {
	"ready": ready,
	"props": ["for"],
	"template": '<small v-if="message" class="help-block" :for=for>{{ message }}</small>',
	"data": function() {
		return {
			"message": null
		};
	}
};

function ready() {
	this.$parent.errors[this.for] = this;
}
