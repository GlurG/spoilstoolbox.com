import $ from "jquery";

import Template from "./submit-button.html";

export default {
	"props": {
		"class":	{ "default": () => ["btn", "btn-primary"] },
		"icon":		[String, Array],
		"text":		String,
		"disabled": {
			"type": Boolean,
			"default": () => false
		}
	},
	"template": Template,
	"data": function() {
		return {
			"load": false
		};
	},
	
	"methods": {
		"ajax": ajax
	}
};

function ajax(options) {
	const icon = this.icon;
	this.icon = null;
	this.load = true;
	this.disabled = true;
	
	return $.ajax(options).always(() => {
		this.icon = icon;
		this.load = false;
		this.disabled = false;
	});
}
