import "selectize";
import $ from "jquery";
import Vue from "vue";

import { L } from "js/config/messages.js";
import { Local } from "js/lib/storage.js";
import { display } from "js/lib/notice.js";
import Form from "js/components/shared/form.js";

import Template from "./card-search-form.html";
import CardSearchHelp from "./card-search-help.js";

export default {
	"ready": ready,
	"props": ["projection", "filters"],
	"template": Template,
	"mixins": [Form],
	"components": {
		"card-search-help": CardSearchHelp
	},

	"data": function() {
		return {
			"query_string": null
		};
	},

	"methods": {
		"open_info": open_info,
		"search": search,
		"search_example": search_example
	}
};

function ready() {
	const filters = this.form.find("select[name='filters']").selectize({
		"plugins": ["remove_button"],
		"render": {
			"option": function(data, escape) {
				switch (data.text) {
					case "Arcanist":	return "<div><img src='/img/icons/arcanist.png'> " + escape(data.text) + "</div>";
					case "Banker":		return "<div><img src='/img/icons/banker.png'> " + escape(data.text) + "</div>";
					case "Gearsmith":	return "<div><img src='/img/icons/gearsmith.png'> " + escape(data.text) + "</div>";
					case "Rogue":		return "<div><img src='/img/icons/rogue.png'> " + escape(data.text) + "</div>";
					case "Warlord":		return "<div><img src='/img/icons/warlord.png'> " + escape(data.text) + "</div>";
					case "Universal":	return "<div><img src='/img/icons/universal.png'> " + escape(data.text) + "</div>";
					default:			return "<div>" + escape(data.text) + "</div>";
				}
			}
		}
	});
	
	filters[0].selectize.setValue(this.filters);
	
	const options = this.form.find("select[name='options']").selectize({
		"plugins": ["remove_button"]
	});

	options[0].selectize.setValue(Local.get("options"));

	options.on("change", () => {
		Local.set("options", options.val());
	});
	
	$("[data-toggle='tooltip']").tooltip();
}

function search() {
	const data = $.extend(this.serialize(), {
		"params": {
			"projection": this.projection
		}
	});

	this.submit({ "data": data }).done(cards => {
		if (!cards) {
			this.$emit("result", []);
			display(L("CARD_SEARCH.EMPTY"));
		} else {
			this.$emit("result", cards);
			display(L("CARD_SEARCH.RESULT", cards.length));
		}
	}).fail(xhr => {
		if (xhr.status === 400) {
			display(L("CARD_SEARCH.ERROR", xhr.responseJSON));
		}
	});
}

function search_example(query_string) {
	this.query_string = query_string;
	Vue.nextTick(() => this.search());
}

function open_info() {
	this.$refs.modal.open();
}
