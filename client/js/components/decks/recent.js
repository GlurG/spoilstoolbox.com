import $ from "jquery";

import Template from "./recent.html";
import Navigation from "./navigation.js";
import ViewMore from "./view-more.js";

export default {
	"route": {
		"data": function(t) {
			$.getJSON("/decks/recent").done(decks => {
				t.next({ "decks": decks });
			});
		}
	},
	
	"template": Template,
	"components": {
		"navigation": Navigation,
		"view-more": ViewMore
	},
	
	"data": function() {
		return {
			"decks": []
		};
	},
	
	"methods": {
		"viewmore": viewmore
	}
};

function viewmore(decks) {
	this.decks = this.decks.concat(decks);
}
