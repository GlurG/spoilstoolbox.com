import $ from "jquery";
import Vue from "vue";

import User from "js/lib/user.js";
import { L } from "js/config/messages.js";
import { display } from "js/lib/notice.js";
import * as Filters from "js/lib/filters.js";

import OCTGN from "./modals/octgn.js";
import Contribute from "./modals/contribute.js";
import Contact from "./modals/contact.js";
import SignIn from "./modals/sign-in.js";
import Register from "./modals/register.js";
import Confirm from "./modals/confirm.js";

import Template from "./application.html";

for (let name in Filters) {
	Vue.filter(name.replace("_", "-"), Filters[name]);
}

export default {
	"replace": false,
	"template": Template,
	"components": {
		"octgn": OCTGN,
		"contribute": Contribute,
		"contact": Contact,
		"sign-in": SignIn,
		"register": Register,
		"confirm": Confirm
	},
	
	"data": function() {
		return {
			"user": User
		};
	},
	
	"computed": {
		"user_icon_style": user_icon_style
	},
	
	"methods": {
		"open": open,
		"sign_out": sign_out
	}
};

function user_icon_style() {
	return this.user.authed ? { "color": "#00AA00" } : {};
}

function open(name) {
	this.$refs[name].open();
}

function sign_out() {
	$.get("/sign-out").done(() => {
		this.user.sign_out();
		display(L("ACCOUNT.SIGNED_OUT"));
	});
}