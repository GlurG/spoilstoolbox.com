import { L } from "js/config/messages.js";
import { display } from "js/lib/notice.js";
import Form from "js/components/shared/form.js";

import Template from "./set-password.html";

export default {
	"route": {
		"data": function(t) {
			t.next({ "token": t.to.params.token });
		}
	},
	
	"ready": ready,
	"template": Template,
	"mixins": [Form],
	"data": function() {
		return {
			"token": null
		};
	}
};

function ready() {
	this.$on("form-success", () => {
		display(L("ACCOUNT.PASSWORD_SET"));
		this.$router.go("/");
	});
}
