/* eslint-disable */

"use strict";

exports.NOTICE_SELECTOR = ".ui-pnotify-text"; 

exports.url = function(path) {
	return `http://localhost:3000/#!${path}`;
};

exports.page_init = function() {
	return $("#application").data("ready") === true;
};

exports.goto = function(path) {
	return function(browser) {
		browser.goto(exports.url(path));
		browser.wait(exports.page_init);
	};
};

exports.visible = function(selector) {
	return $(selector).is(":visible");
};

exports.submit_form = function(form_id, form_data) {
	return function(browser) {
		for (var field_name in form_data) {
			var field_selector = `${form_id} input[name=${field_name}]`;
			var field_value = form_data[field_name];
			browser.insert(field_selector, field_value);
		}

		var submit_button = `${form_id} .btn-primary`;
		var form_wait = function(form_id) {
			var state = $(form_id).data("state");
			return state === "success" || state === "fail";
		};

		browser.click(submit_button).wait(form_wait, form_id);
	};
};

exports.collect_response = function() {
	return function(browser) {
		browser.evaluate(NOTICE_SELECTOR => {
			var result = {
				"url": document.location.href,
				"form": {},
				"notice": $(NOTICE_SELECTOR).html()
			};

			$("small.help-block").map(function() {
				result.form[$(this).attr("for")] = $(this).text();
			});

			return result;
		}, exports.NOTICE_SELECTOR);
	};
};

exports.run = function(callback) {
	return function(browser) {
		browser.run((err, result) => {
			if (err) {
				console.error(err);
			} else {
				callback(result);
			}
		});
	};
};
