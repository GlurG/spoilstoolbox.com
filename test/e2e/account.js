"use strict";

const test = require("tape");
const Browser = require("nightmare");

const main = require("../../server/main");
const common = require("./common");
const actions = require("./account-actions");
const Accounts = require("../../server/models/accounts");
const Lookup = require("../../client/js/config/messages");
const L = function(code, ...args) {
	const message = Lookup(code, args);
	return typeof(message) === "string" ? message : message.text;
};

const BROWSER_OPTIONS = { "show": true };

test("INIT", t => {
	main.init(() => {
		Browser(BROWSER_OPTIONS).cookies.clear().run(() => t.end()).end();
	});
});

test("Attempt to register account without providing anything", t => {
	Browser(BROWSER_OPTIONS)
		.use(common.goto("/"))
		.use(actions.register_account())
		.use(common.run(result => {
			t.equal(result.form.email, L("ACCOUNT.EMAIL_INVALID"));
			t.equal(result.form.username, L("ACCOUNT.USERNAME_INVALID_LENGTH"));
			t.equal(result.form.password, L("ACCOUNT.PASSWORD_TOO_SHORT"));
			t.end();
		})).end();
});

test("Attempt to register with a disposable email and invalid username", t => {
	Browser(BROWSER_OPTIONS)
		.use(common.goto("/"))
		.use(actions.register_account("test@mailinator.com", "%&user!¤", "password"))
		.use(common.run(result => {
			t.equal(result.form.email, L("ACCOUNT.EMAIL_DISPOSABLE"));
			t.equal(result.form.username, L("ACCOUNT.USERNAME_ALNUM_ONLY"));
			t.end();
		})).end();
});

const EMAIL = "test@gmail.com";
const USERNAME = "test";
const PASSWORD = "password";

let activation_token;

test("Register account", t => {
	Browser(BROWSER_OPTIONS)
		.use(common.goto("/"))
		.use(actions.register_account(EMAIL, USERNAME, PASSWORD))
		.use(common.run(result => {
			t.equal(result.notice, L("ACCOUNT.CREATED"));

			Accounts.find_by_email(EMAIL, (err, account) => {
				activation_token = account.activation_token;
				t.end();
			});
		})).end();
});

test("Register account with same email and username", t => {
	Browser(BROWSER_OPTIONS)
		.use(common.goto("/"))
		.use(actions.register_account(EMAIL, USERNAME, PASSWORD))
		.use(common.run(result => {
			t.equal(result.form.email, L("ACCOUNT.EMAIL_REGISTERED"));
			t.equal(result.form.username, L("ACCOUNT.USERNAME_REGISTERED"));
			t.end();
		})).end();
});

test("Activate account with an invalid token", t => {
	Browser(BROWSER_OPTIONS)
		.use(actions.activate_account("invalid-token"))
		.use(common.run(result => {
			t.equal(result.url, common.url("/"));
			t.equal(result.notice, L("ACCOUNT.ACTIVATION_FAILED"));
			t.end();
		})).end();
});

test("Attempt sign in without providing any credentials", t => {
	Browser(BROWSER_OPTIONS)
		.use(common.goto("/"))
		.use(actions.sign_in())
		.use(common.run(result => {
			t.equal(result.form.username, L("ACCOUNT.USERNAME_INVALID_LENGTH"));
			t.equal(result.form.password, L("ACCOUNT.PASSWORD_TOO_SHORT"));
			t.end();
		})).end();
});

test("Attempt sign in with invalid credentials", t => {
	Browser(BROWSER_OPTIONS)
		.use(common.goto("/"))
		.use(actions.sign_in("invaliduser", "boguspassword"))
		.use(common.run(result => {
			t.equal(result.notice, L("ACCOUNT.INVALID_CREDENTIALS"));
			t.end();
		})).end();
});

test("Attempt sign in to inactive account with correct credentials", t => {
	Browser(BROWSER_OPTIONS)
		.use(common.goto("/"))
		.use(actions.sign_in(USERNAME, EMAIL))
		.use(common.run(result => {
			t.equal(result.notice, L("ACCOUNT.NOT_ACTIVATED"));
			t.end();
		})).end();
});

test("Activate account", t => {
	Browser(BROWSER_OPTIONS)
		.use(actions.activate_account(activation_token))
		.use(common.run(result => {
			t.equal(result.url, common.url("/"));
			t.equal(result.notice, L("ACCOUNT.ACTIVATED"));
			t.end();
		})).end();
});

test("Attempt to access account/settings without being logged in", t => {
	Browser(BROWSER_OPTIONS)
		.use(common.goto("/account/settings"))
		.use(common.collect_response())
		.use(common.run(result => {
			t.equal(result.url, common.url("/"));
			t.equal(result.notice, L("HTTP.401"));
			t.end();
		})).end();
});

test("Sign in", t => {
	Browser(BROWSER_OPTIONS)
		.use(common.goto("/"))
		.use(actions.sign_in(USERNAME, PASSWORD))
		.use(common.run(result => {
			t.equal(result.notice, L("ACCOUNT.SIGNED_IN"));
			t.end();
		})).end();
});

test("Sign in and goto account/settings", t => {
	Browser(BROWSER_OPTIONS)
		.use(common.goto("/"))
		.use(actions.sign_in(USERNAME, PASSWORD))
		.use(common.goto("/account/settings"))
		.visible("#profile-form")
		.use(common.run(visible => {
			t.equal(visible, true);
			t.end();
		})).end();
});

test("Attempt to change password without providing anything", t => {
	Browser(BROWSER_OPTIONS)
		.use(common.goto("/"))
		.use(actions.sign_in(USERNAME, PASSWORD))
		.use(actions.change_password())
		.use(common.run(result => {
			t.equal(result.form.current_password, L("ACCOUNT.PASSWORD_TOO_SHORT"));
			t.equal(result.form.new_password, L("ACCOUNT.PASSWORD_TOO_SHORT"));
			t.end();
		})).end();
});

test("Attempt to change password but providing an incorrect current password", t => {
	Browser(BROWSER_OPTIONS)
		.use(common.goto("/"))
		.use(actions.sign_in(USERNAME, PASSWORD))
		.use(actions.change_password("incorrect-password", "password"))
		.use(common.run(result => {
			t.equal(result.notice, L("ACCOUNT.CURRENT_PASSWORD_MISMATCH"));
			t.end();
		})).end();
});

test("Recover account with invalid username", t => {
	Browser(BROWSER_OPTIONS)
		.use(common.goto("/"))
		.use(actions.recover_account("invaliduser"))
		.use(common.run(result => {
			t.equal(result.form.email_or_username, L("ACCOUNT.BY_USERNAME_NOT_FOUND"));
			t.end();
		})).end();
});

test("Recover account with invalid email", t => {
	Browser(BROWSER_OPTIONS)
		.use(common.goto("/"))
		.use(actions.recover_account("invalid@email.com"))
		.use(common.run(result => {
			t.equal(result.form.email_or_username, L("ACCOUNT.BY_EMAIL_NOT_FOUND"));
			t.end();
		})).end();
});

let recovery_token;

test("Recover account by email", t => {
	Browser(BROWSER_OPTIONS)
		.use(common.goto("/"))
		.use(actions.recover_account(EMAIL))
		.use(common.run(result => {
			t.equal(result.notice, L("ACCOUNT.RECOVERY_EMAIL_SENT"));

			Accounts.find_by_email(EMAIL, (err, account) => {
				recovery_token = account.recovery_token;
				t.end();
			});
		})).end();
});

test("Attempt to set new password with an empty password and invalid token", t => {
	Browser(BROWSER_OPTIONS)
		.use(actions.set_password("invalid-token"))
		.use(common.run(result => {
			t.equal(result.form.password, L("ACCOUNT.PASSWORD_TOO_SHORT"));
			t.end();
		})).end();
});

test("Attempt to set new password with an invalid token", t => {
	Browser(BROWSER_OPTIONS)
		.use(actions.set_password("invalid-token", "new-password"))
		.use(common.run(result => {
			t.equal(result.notice, L("ACCOUNT.INVALID_RECOVERY_TOKEN"));
			t.end();
		})).end();
});

test("Set new password", t => {
	Browser(BROWSER_OPTIONS)
		.use(actions.set_password(recovery_token, "new-password"))
		.use(common.run(result => {
			t.equal(result.notice, L("ACCOUNT.PASSWORD_SET"));
			t.end();
		})).end();
});

test("Sign in with new password", t => {
	Browser(BROWSER_OPTIONS)
		.use(common.goto("/"))
		.use(actions.sign_in(USERNAME, "new-password"))
		.use(common.run(result => {
			t.equal(result.notice, L("ACCOUNT.SIGNED_IN"));
			t.end();
		})).end();
});

test("SHUTDOWN", t => {
	main.shutdown(() => {
		t.end();
	});
});
