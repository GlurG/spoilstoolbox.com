"use strict";

const test = require("tape");
const request = require("supertest");

const main = require("../../server/main");
let server;

function is_sorted_by(field) {
	return function(card, i, array) {
		let v1 = array[i][field];
		v1 = v1 === undefined ? true : v1;
		let v2 = card[field];
		v2 = v2 === undefined ? true : v2;

		return i === 0 || v1 <= v2;
	};
}

function is_empty(object) {
	return Object.keys(object).length === 0;
}

function test_query() {
	// http://stackoverflow.com/questions/960866/how-can-i-convert-the-arguments-object-to-an-array-in-javascript
	const args = [].slice.call(arguments);
	const evaluate = args.pop(); // Always last
	const query = { "params": { "projection": "full" } };

	switch (args.length) {
		case 1: {
			const query_type = typeof(args[0]) === "string" ? "query_string" : "filters";
			query[query_type] = args[0];
			break;
		}

		case 2:
			if (typeof(args[0]) === "string") {
				query.query_string = args[0];
				query.filters = args[1];
			} else {
				query.filters = args[0];
				query.options = args[1];
			}
			break;

		case 3:
			query.query_string = args[0];
			query.filters = args[1];
			query.options = args[2];
			break;
	}
	
	test(JSON.stringify(query), t => {
		request(server)
			.post("/cards/search")
			.send(query)
			.end((err, result) => {
				t.assert(evaluate(result.body, result.status));
				t.end();
			});
	});
}

test("INIT", t => {
	main.init((err, app) => {
		server = app;
		t.end();
	});
});

// cost, strength, life, speed, structure have the same code path
test_query("cost = 10",
	cards => cards.every(card => card.cost === 10));
test_query("cost > 9",
	cards => cards.every(card => card.cost > 9));
test_query("cost < 1",
	cards => cards.every(card => card.cost < 1));
test_query("cost > 8 cost <= 10",
	cards => cards.every(card => card.cost > 8 && card.cost <= 10));
test_query("cost >= 8 cost < 10",
	cards => cards.every(card => card.cost >= 8 && card.cost < 10));
test_query("cost = 0 cost > 5", // = will override all other operators
	cards => cards.every(card => card.cost === 0));

test_query("threshold = 4",
	cards => cards.every(card => card.threshold.length === 4));
test_query("threshold > 3",
	cards => cards.every(card => card.threshold.length > 3));
test_query("threshold < 2",
	cards => cards.every(card => card.threshold.length < 2));
test_query("threshold > 2 threshold <= 4",
	cards => cards.every(card => card.threshold.length > 2 && card.threshold.length <= 4));
test_query("threshold >= 2 threshold < 4",
	cards => cards.every(card => card.threshold.length >= 2 && card.threshold.length < 4));
test_query("threshold = 4 threshold < 2", // = will override all other operators
	cards => cards.every(card => card.threshold.length === 4));

test_query("arcanist tactic uncommon",
	cards => cards.every(card => card.trade === "arcanist" && card.type === "tactic" && card.rarity === "uncommon"));

// Filter version of the same query
test_query(["arcanist", "tactic", "uncommon"],
	cards => cards.every(card => card.trade === "arcanist" && card.type === "tactic" && card.rarity === "uncommon"));

// Filter by set
const SEED = "abf7b04c-55cd-4f21-b02b-dc403a04c128";
const HOLY_HEIST = "74be93a3-2f0f-44cc-9162-fbd6e5b42ef5";
test_query([SEED, HOLY_HEIST],
	cards => cards.every(card => card.set.id === SEED || card.set.id === HOLY_HEIST));

// Search for a 'special' card
const SPECIAL_CARD = "The Evil Alliance";
test_query(SPECIAL_CARD,
	cards => cards[0]["name"] === SPECIAL_CARD);
test_query(SPECIAL_CARD, ["Chronicles"],
	(cards, status) => is_empty(cards) && status === 204);
test_query(SPECIAL_CARD, ["Chronicles"], ["include_specials"],
	cards => cards[0]["name"] === SPECIAL_CARD);

// Search for a card that is banned in Chronicles and not legal in Current
const BANNED_CARD = "Emergency Obfuscation";
test_query(BANNED_CARD,
	cards => cards[0]["name"] === BANNED_CARD);
test_query(BANNED_CARD, ["Chronicles"],
	(cards, status) => is_empty(cards) && status === 204);
test_query(BANNED_CARD, ["Chronicles"], ["include_bans"],
	cards => cards[0]["name"] === BANNED_CARD);

// Search for a reprinted card
test_query("Research Investment", null, ["include_reprints"],
	cards => cards.length > 1);

// Sort by...
test_query([10], ["sort_by_cost"],
	cards => cards.every(is_sorted_by("cost")));
test_query([10], ["sort_by_threshold"],
	cards => cards.every(is_sorted_by("threshold"))); // We get away with this, since "X" <= "XX"
test_query([10], ["sort_by_strength"],
	cards => cards.every(is_sorted_by("strength")));
test_query([10], ["sort_by_life"],
	cards => cards.every(is_sorted_by("life")));
test_query([10], ["sort_by_speed"],
	cards => cards.every(is_sorted_by("speed")));
test_query([10], ["sort_by_structure"],
	cards => cards.every(is_sorted_by("structure")));

// Search for Current card with Current filter
test_query("Juggernauts", ["Current"],
	cards => cards[0]["name"] === "Juggernauts");

// Search for a Chronicles card with Current filter
test_query("Spry Archer", ["Current"],
	(cards, status) => is_empty(cards) && status === 204);

// Card with l33t name
test_query("0p3r4710n 1337 H4x0rz",
	cards => cards[0]["name"] === "0p3r4710n 1337 H4x0rz");

// Automatic 1337-speak conversion
const TOOLBOX_ELF = "700160x 31f";
test_query("Toolbox elf",
	cards => cards[0]["name"] === TOOLBOX_ELF);

// Search for incomplete card name (all searches are wildcard searches)
test_query("Cantan",
	cards => cards[0]["name"] === "Cantankerous Claywork");

// Search for incomplete leet name
test_query("Toolb",
	cards => cards[0]["name"] === TOOLBOX_ELF);

// Phrase
const PHRASE = "at the start of your turn";
test_query(`"${PHRASE}"`,
	cards => cards.every(card => card.text.match(RegExp(PHRASE, "ig")) !== null));

// Exclude
test_query("Selective Gluttony -discard",
	(cards, status) => is_empty(cards) && status === 204);

// Apostrophe
const SEARCH_WITH_APOSTROPHE = "Gideon's";
test_query(SEARCH_WITH_APOSTROPHE,
	cards => cards.every(card => card.name.match(RegExp(SEARCH_WITH_APOSTROPHE, "i")) !== null));

test("SHUTDOWN", t => {
	main.shutdown(() => {
		t.end();
	});
});
