upstream spoilstoolbox {
	ip_hash;
	server					127.0.0.1:3000;
}

server {
	listen					443 deferred ssl http2;
	listen					[::]:443 deferred ssl http2;
	server_name				spoilstoolbox.com www.spoilstoolbox.com;
	root					%(root_dir)s;

	ssl_certificate			/etc/letsencrypt/live/spoilstoolbox.com/fullchain.pem;
	ssl_certificate_key		/etc/letsencrypt/live/spoilstoolbox.com/privkey.pem;

	include					ssl.conf;
	include					expires.conf;

	location / {
		try_files			/maintenance.html $uri @proxy;
	}

	location	@proxy {
		proxy_set_header	X-Real-IP $remote_addr;
		proxy_set_header	X-Forwarded-For $proxy_add_x_forwarded_for;
		proxy_set_header	Host $http_host;
		proxy_set_header	X-NginX-Proxy true;

		proxy_pass			http://spoilstoolbox;
		proxy_redirect		off;
	}

	add_header				"X-UA-Compatible" "IE=Edge,chrome=1";

	# Security stuff from: https://github.com/h5bp/server-configs-nginx/blob/master/h5bp/directive-only/extra-security.conf
	# These add_header directives needs to be here because: http://stackoverflow.com/questions/18450310/nginx-add-header-not-working
	add_header X-Frame-Options SAMEORIGIN;
	add_header X-Content-Type-Options nosniff;
	add_header X-XSS-Protection "1; mode=block";

	# Show real IP from Cloud Flare visitors
	set_real_ip_from		199.27.128.0/21;
	set_real_ip_from		173.245.48.0/20;
	set_real_ip_from		103.21.244.0/22;
	set_real_ip_from		103.22.200.0/22;
	set_real_ip_from		103.31.4.0/22;
	set_real_ip_from		141.101.64.0/18;
	set_real_ip_from		108.162.192.0/18;
	set_real_ip_from		190.93.240.0/20;
	set_real_ip_from		188.114.96.0/20;
	set_real_ip_from		197.234.240.0/22;
	set_real_ip_from		198.41.128.0/17;
	set_real_ip_from		162.158.0.0/15;
	set_real_ip_from		2400:cb00::/32;
	set_real_ip_from		2606:4700::/32;
	set_real_ip_from		2803:f800::/32;
	set_real_ip_from		2405:b500::/32;
	set_real_ip_from		2405:8100::/32;
	real_ip_header			CF-Connecting-IP;
}
