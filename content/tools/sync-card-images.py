import glob, os
from PIL import Image, ImageFile

ImageFile.MAXBLOCK = 2**20
CARD_SIZE = (300, 420)

SCRIPT_PATH = os.path.dirname(os.path.realpath(__file__))
SRC_PATH = os.path.join(SCRIPT_PATH, "..", "cards", "sets")
WEB_PATH = os.path.join(SCRIPT_PATH, "..", "..", "client", "img", "cards")

def sync(src_file, dest_file):
	print("Syncing '%s'..." % os.path.basename(src_file))
	image = Image.open(src_file)
	image = image.resize(CARD_SIZE, Image.ANTIALIAS)
	image.save(dest_file, "JPEG", quality = 95, optimize = True, progressive = True)

synced = []

for src_file in glob.glob(os.path.join(SRC_PATH, "**", "*.jpg")):
	src_file_name = os.path.basename(src_file)
	dest_file = os.path.join(WEB_PATH, src_file_name)
	synced.append(dest_file)

	if not os.path.exists(dest_file) or os.stat(src_file).st_mtime > os.stat(dest_file).st_mtime:
		sync(src_file, dest_file)

web_images = glob.glob(os.path.join(WEB_PATH, "*.jpg"))
dangling = set(web_images) - set(synced)

for card_image in dangling:
	os.remove(card_image)

print("%d dangling images cleaned up" % len(dangling))
