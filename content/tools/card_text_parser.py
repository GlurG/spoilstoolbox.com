import string, re
from itertools import groupby

KEYWORDS_TO_TEXT = {
	"TACTICAL": "You may play this card whenever you could play a tactic.",
	"AGGRO": "This card can attack even if it has not been in play under your control since the start of the turn.",
	# This is slightly messed up since this text is formatted as "FEALTY: <rules-text>"
	"FEALTY": "FEALTY: While this card is in your hand and you control at least 4 staple resources of different trades, reduce this card's threshold requirement by any 3 resource icons."
}

SKIP_TOKENS = [":"]

def parse_card_text(card_text):
	keywords, card_text = text_to_keyword(card_text)
	tokens = tokenize(card_text)
	return TokenParser(keywords).parse(tokens)

def text_to_keyword(card_text):
	keywords = []

	for keyword, text in KEYWORDS_TO_TEXT.items():
		if text in card_text:
			card_text = card_text.replace(text, "")
			keywords.append(keyword)
	
	return keywords, card_text.strip()

def tokenize(card_text):
	token = ""
	tokens = []
	char_iter = iter(card_text)

	for char in card_text:
		if char in SKIP_TOKENS:
			continue
		elif char in string.whitespace:
			if len(token) == 0:
				# Ending up here means we have read consecutive whitespaces,
				# which we discard
				continue
			
			tokens.append(token)

			if char == "\n":
				tokens.append("\n")
			
			token = ""
		else:
			token += char
	
	tokens.append(token)

	return [token for token in tokens if token] # Removes stray empty strings

class TokenParser:
	CONDITIONALS = ["while"]
	TRIGGERS = ["if", "when", "at"]

	def __init__(self, keywords):
		self.abilities		= []
		self.extra_costs	= []
		self.keywords		= keywords
		self.effects		= []
		self.triggers		= []
		self.conditionals	= []

	def parse(self, tokens):
		while len(tokens) > 0:
			line = self.read_line(tokens)
			self.parse_line(tokens, line)

		parsed_text = {}

		if len(self.abilities):
			parsed_text.update({ "abilities": self.abilities })
		if len(self.extra_costs):
			parsed_text.update({ "extra_costs": self.extra_costs })
		if len(self.keywords):
			parsed_text.update({ "keywords": self.keywords })
		if len(self.effects):
			parsed_text.update({ "effects": self.effects })
		if len(self.triggers):
			parsed_text.update({ "triggers": self.triggers })
		if len(self.conditionals):
			parsed_text.update({ "conditionals": self.conditionals })

		return parsed_text

	def read_line(self, tokens):
		line = []

		while len(tokens) > 0:
			token = tokens.pop(0)

			if token == "\n":
				break;
			else:
				line.append(token)

		return line

	def parse_line(self, tokens, line):
		def match_all(tokens, *args):
			for arg in args:
				if arg not in tokens:
					return False

			return True

		def match_any(tokens, *args):
			for arg in args:
				if arg in tokens:
					return True

			return False

		if match_all(line, "EXTRA", "COST"):
			extra_cost = " ".join(line[2:])
			self.extra_costs.append(extra_cost)

			if len(tokens) > 0:
				effect_line = self.read_line(tokens)
				effect = " ".join(effect_line)
				self.effects.append(effect)
		elif all(token.isupper() for token in line) and match_any(line, "ATTACH", "UNIQUE", "COVERT", "AGGRO", "TACTICAL", "PROVIDES"):
			keyword = " ".join(line)
			self.keywords.append(keyword)
		elif re.match("\[\w\]+", line[0]) != None:
			trigger = " ".join(line[1:])
			self.triggers.append(trigger)
		elif match_all(line, "FLIP", "UP"):
			keyword = " ".join(line[0:2])
			self.keywords.append(keyword)

			try:
				aux = line.index("and") + 1
				extra_cost = " ".join(line[aux:])
				self.extra_costs.append(extra_cost)
			except ValueError:
				pass
		elif match_all(line, "RECUR"):
			keyword = line[0]
			self.keywords.append(keyword)
			cost_line = self.read_line(tokens)
			extra_cost = " ".join(cost_line[1:])
			self.extra_costs.append(extra_cost)
			effect_line = self.read_line(tokens)
			effect = " ".join(effect_line[1:])
			self.effects.append(effect)
		elif match_all(line, "OPTION"):
			keyword = " ".join(line)
			next_line = self.read_line(tokens)
			if match_all(next_line, "EXTRA", "COST"):
				self.parse_line(tokens, next_line)
			else:
				effect = " ".join(next_line)
				self.effects.append(effect)
		# Meat of the Mountain
		elif line[0].isdigit() and "TOKEN" in line[1]:
			trigger = " ".join(line[2:])
			self.triggers.append(trigger)
		# Meat of the Mountain
		elif match_all(line, "AT", "LEAST", "3", "TOKENS"):
			trigger = " ".join(line[4:])
			self.triggers.append(trigger)
		elif line[0].lower() in TokenParser.CONDITIONALS:
			conditional = " ".join(line)
			self.conditionals.append(conditional)
		elif line[0].lower() in TokenParser.TRIGGERS:
			trigger = " ".join(line)
			self.triggers.append(trigger)
		# We are hopefully parsing an ability here
		elif all(token.isupper() for token in line):
			name = " ".join(line)
			cost_line = self.read_line(tokens)
			cost = " ".join(cost_line[1:])
			effect_line = self.read_line(tokens)
			effect = " ".join(effect_line[1:])
			self.abilities.append({
				"name": name,
				"cost": cost,
				"effect": effect
			})
		# We are hopefully parsing some general rules text here.
		# Knobby Stave: "This card enters play with..."
		else:
			effect = " ".join(line)
			self.effects.append(effect)
